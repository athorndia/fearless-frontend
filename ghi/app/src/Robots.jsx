/*
Auto-generated by: https://github.com/pmndrs/gltfjsx
Command: npx gltfjsx@6.2.15 C:\Users\aktho\OneDrive\Desktop\Hack-Reactor\projects\fearless-frontend\ghi\app\public\robots\scene.gltf 
Author: Marcos Faci (https://sketchfab.com/marcosfaci)
License: SKETCHFAB Standard (https://sketchfab.com/licenses)
Source: https://sketchfab.com/3d-models/set-of-robots-1124b3a3a15d4048aa6c35b01b5aae16
Title: Set of Robots 🤖
*/

import React, { useRef, useEffect } from 'react'
import { useGLTF, useAnimations } from '@react-three/drei'
import { Canvas } from "@react-three/fiber";

const Robots = (props) => {

  const group = useRef()
  const { nodes, materials, animations } = useGLTF('/robots/scene.gltf')
  const { actions } = useAnimations(animations, group)

  useEffect(() => {
    let animationIndex = 0;

    const playNextAnimation = () => {
      const nextAnimation = animations[animationIndex];
      const action = actions[nextAnimation.name];
      action.play();

      animationIndex = (animationIndex + 1) % animations.length;
  };

  const intervalId = setInterval(playNextAnimation, 1000); // Change interval time as per your requirement

  return () => clearInterval(intervalId);
  }, [actions, animations]);
  
  useEffect(() => {
    const handleResize = () => {
      if (window.innerWidth < 768) {
        group.current.scale.set(0.5, 0.5, 0.5);
      } else {
        group.current.scale.set(1, 1, 1);
      }
    };

    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, [group]);

  return (
    <Canvas>
      <ambientLight intensity={0.5} />
      <group ref={group} {...props} dispose={null}>
        <group name="Sketchfab_Scene">
          <group name="Sketchfab_model" rotation={[-Math.PI / 2, 0, 0]}>
            <group name="1295dcf0c61449f4b0698ae7e46e5fabfbx" rotation={[Math.PI / 2, 0, 0]} scale={0.01}>
              <group name="Object_2">
                <group name="RootNode">
                  <group name="bot_B" position={[0, -0.397, 0]}>
                    <group name="bot_B_arm_l" position={[5.081, 12.334, 0.547]} rotation={[-3.095, -0.093, 0.092]} scale={-1}>
                      <mesh name="bot_B_arm_l_bot_B_texture_0" geometry={nodes.bot_B_arm_l_bot_B_texture_0.geometry} material={materials.bot_B_texture} />
                    </group>
                    <group name="bot_B_arm_r" position={[-5.068, 12.334, 0.547]} rotation={[0.123, 0, 0.136]}>
                      <mesh name="bot_B_arm_r_bot_B_texture_0" geometry={nodes.bot_B_arm_r_bot_B_texture_0.geometry} material={materials.bot_B_texture} />
                    </group>
                    <group name="bot_B_head" position={[0, 14.571, 0]} rotation={[0.036, -0.01, -0.016]}>
                      <group name="bot_B_face" position={[0, 0, -0.1]}>
                        <mesh name="bot_B_face_face_02_0" geometry={nodes.bot_B_face_face_02_0.geometry} material={materials.face_02} />
                      </group>
                      <mesh name="bot_B_head_bot_B_texture_0" geometry={nodes.bot_B_head_bot_B_texture_0.geometry} material={materials.bot_B_texture} />
                      <mesh name="bot_B_head_light_blue_0" geometry={nodes.bot_B_head_light_blue_0.geometry} material={materials.light_blue} />
                      <mesh name="bot_B_head_transparency_crystal_0" geometry={nodes.bot_B_head_transparency_crystal_0.geometry} material={materials.transparency_crystal} />
                      <mesh name="bot_B_head_face_02_0" geometry={nodes.bot_B_head_face_02_0.geometry} material={materials.face_02} />
                    </group>
                    <mesh name="bot_B_bot_B_texture_0" geometry={nodes.bot_B_bot_B_texture_0.geometry} material={materials.bot_B_texture} />
                    <mesh name="bot_B_light_blue_0" geometry={nodes.bot_B_light_blue_0.geometry} material={materials.light_blue} />
                    <mesh name="bot_B_transparency_crystal_0" geometry={nodes.bot_B_transparency_crystal_0.geometry} material={materials.transparency_crystal} />
                  </group>
                  <group name="bot_D_scale_fullAnim" position={[-62.387, 0, 0]} scale={4.138}>
                    <group name="bot_D" position={[0, 2.672, 2.949]} rotation={[-0.015, 0, 0]}>
                      <group name="bot_D_head" position={[0, 0.018, 0]}>
                        <group name="propeller" position={[0, -1.02, -1.557]}>
                          <mesh name="propeller_light_blue_0" geometry={nodes.propeller_light_blue_0.geometry} material={materials.light_blue} />
                          <mesh name="propeller_bot_D_texture_0" geometry={nodes.propeller_bot_D_texture_0.geometry} material={materials.bot_D_texture} />
                        </group>
                        <group name="wing_left">
                          <mesh name="wing_left_bot_D_texture_0" geometry={nodes.wing_left_bot_D_texture_0.geometry} material={materials.bot_D_texture} />
                          <mesh name="wing_left_bot_D_light_0" geometry={nodes.wing_left_bot_D_light_0.geometry} material={materials.bot_D_light} />
                        </group>
                        <group name="wing_right">
                          <mesh name="wing_right_bot_D_texture_0" geometry={nodes.wing_right_bot_D_texture_0.geometry} material={materials.bot_D_texture} />
                          <mesh name="wing_right_bot_D_light_0" geometry={nodes.wing_right_bot_D_light_0.geometry} material={materials.bot_D_light} />
                        </group>
                        <group name="bot_D_face" position={[0, -0.001, 1.803]}>
                          <mesh name="bot_D_face_face_03_0" geometry={nodes.bot_D_face_face_03_0.geometry} material={materials.face_03} />
                        </group>
                        <group name="bot_D_face_static" position={[0, -0.001, 1.834]}>
                          <mesh name="bot_D_face_static_face_03_0" geometry={nodes.bot_D_face_static_face_03_0.geometry} material={materials.face_03} />
                        </group>
                        <group name="ear_inclination_right" position={[-1.369, 1.89, -0.359]} rotation={[0, 0, 0.627]}>
                          <group name="ear_rot_right" position={[0, 0, 0.001]} rotation={[-1.615, 0, 0]}>
                            <mesh name="ear_rot_right_bot_D_texture_0" geometry={nodes.ear_rot_right_bot_D_texture_0.geometry} material={materials.bot_D_texture} />
                          </group>
                          <mesh name="ear_inclination_right_bot_D_texture_0" geometry={nodes.ear_inclination_right_bot_D_texture_0.geometry} material={materials.bot_D_texture} />
                        </group>
                        <group name="ear_inclination_left" position={[1.37, 1.89, -0.359]} rotation={[0, 0, -0.627]}>
                          <group name="ear_rot_left" position={[0, 0, 0.001]} rotation={[-1.615, -0.012, -0.001]}>
                            <mesh name="ear_rot_left_bot_D_texture_0" geometry={nodes.ear_rot_left_bot_D_texture_0.geometry} material={materials.bot_D_texture} />
                          </group>
                          <mesh name="ear_inclination_left_bot_D_texture_0" geometry={nodes.ear_inclination_left_bot_D_texture_0.geometry} material={materials.bot_D_texture} />
                        </group>
                        <mesh name="bot_D_head_bot_D_texture_0" geometry={nodes.bot_D_head_bot_D_texture_0.geometry} material={materials.bot_D_texture} />
                        <mesh name="bot_D_head_light_blue_0" geometry={nodes.bot_D_head_light_blue_0.geometry} material={materials.light_blue} />
                        <mesh name="bot_D_head_transparency_crystal_0" geometry={nodes.bot_D_head_transparency_crystal_0.geometry} material={materials.transparency_crystal} />
                      </group>
                      <group name="leg_left_F" position={[0.556, -0.497, 0.274]} rotation={[0, 0, 0.608]}>
                        <group name="leg_left_F_1" position={[0, -1.085, 0]} rotation={[2.71, 0.451, -3.017]}>
                          <group name="leg_left_F_2" position={[-0.013, -0.88, 0]} rotation={[0, 0, -0.035]}>
                            <group name="leg_left_F_3" position={[-0.862, -0.002, 0]} rotation={[0, 0, -1.16]}>
                              <mesh name="leg_left_F_3_bot_D_texture_0" geometry={nodes.leg_left_F_3_bot_D_texture_0.geometry} material={materials.bot_D_texture} />
                            </group>
                            <mesh name="leg_left_F_2_bot_D_texture_0" geometry={nodes.leg_left_F_2_bot_D_texture_0.geometry} material={materials.bot_D_texture} />
                          </group>
                          <mesh name="leg_left_F_1_bot_D_texture_0" geometry={nodes.leg_left_F_1_bot_D_texture_0.geometry} material={materials.bot_D_texture} />
                        </group>
                      </group>
                      <group name="leg_left_B" position={[0.556, -0.497, -0.274]} rotation={[0, 0, 0.608]}>
                        <group name="leg_left_B_1" position={[0, -1.085, 0]} rotation={[3.006, 0.191, -3.133]}>
                          <group name="leg_left_B_2" position={[-0.013, -0.88, 0]} rotation={[0, 0, -0.035]}>
                            <group name="leg_left_B_3" position={[-0.862, -0.002, 0]} rotation={[0, 0, -1.16]}>
                              <mesh name="leg_left_B_3_bot_D_texture_0" geometry={nodes.leg_left_B_3_bot_D_texture_0.geometry} material={materials.bot_D_texture} />
                            </group>
                            <mesh name="leg_left_B_2_bot_D_texture_0" geometry={nodes.leg_left_B_2_bot_D_texture_0.geometry} material={materials.bot_D_texture} />
                          </group>
                          <mesh name="leg_left_B_1_bot_D_texture_0" geometry={nodes.leg_left_B_1_bot_D_texture_0.geometry} material={materials.bot_D_texture} />
                        </group>
                      </group>
                      <group name="leg_right_B" position={[-0.564, -0.497, -0.274]} rotation={[-Math.PI, 0, -2.534]}>
                        <group name="leg_right_B_1" position={[0, -1.085, 0]} rotation={[2.927, 0.302, 3.125]}>
                          <group name="leg_right_B_2" position={[-0.013, -0.88, 0]} rotation={[-0.004, -0.003, -0.164]}>
                            <group name="leg_right_B_3" position={[-0.862, -0.002, 0]} rotation={[0, 0, -1.16]}>
                              <mesh name="leg_right_B_3_bot_D_texture_0" geometry={nodes.leg_right_B_3_bot_D_texture_0.geometry} material={materials.bot_D_texture} />
                            </group>
                            <mesh name="leg_right_B_2_bot_D_texture_0" geometry={nodes.leg_right_B_2_bot_D_texture_0.geometry} material={materials.bot_D_texture} />
                          </group>
                          <mesh name="leg_right_B_1_bot_D_texture_0" geometry={nodes.leg_right_B_1_bot_D_texture_0.geometry} material={materials.bot_D_texture} />
                        </group>
                      </group>
                      <group name="leg_right_B1" position={[-0.564, -0.497, 0.274]} rotation={[-Math.PI, 0, -2.534]}>
                        <group name="leg_right_B_1_1" position={[0, -1.085, 0]} rotation={[-2.871, -0.366, 3.12]}>
                          <group name="leg_right_B_2_1" position={[-0.013, -0.88, 0]} rotation={[0, 0, -0.17]}>
                            <group name="leg_right_B_3_1" position={[-0.862, -0.002, 0]} rotation={[0, 0, -1.16]}>
                              <mesh name="leg_right_B_3_bot_D_texture_0_1" geometry={nodes.leg_right_B_3_bot_D_texture_0_1.geometry} material={materials.bot_D_texture} />
                            </group>
                            <mesh name="leg_right_B_2_bot_D_texture_0_1" geometry={nodes.leg_right_B_2_bot_D_texture_0_1.geometry} material={materials.bot_D_texture} />
                          </group>
                          <mesh name="leg_right_B_1_bot_D_texture_0_1" geometry={nodes.leg_right_B_1_bot_D_texture_0_1.geometry} material={materials.bot_D_texture} />
                        </group>
                      </group>
                    </group>
                  </group>
                  <group name="bot_A" position={[-31.062, 0, -32.295]}>
                    <group name="bot_A_head" position={[0, 14.256, 32.372]} rotation={[-0.003, -0.296, 0.001]}>
                      <group name="bot_A_face" position={[0, 0, 0.05]}>
                        <mesh name="bot_A_face_face_01_0" geometry={nodes.bot_A_face_face_01_0.geometry} material={materials.face_01} />
                      </group>
                      <mesh name="bot_A_head_bot_A_texture_0" geometry={nodes.bot_A_head_bot_A_texture_0.geometry} material={materials.bot_A_texture} />
                      <mesh name="bot_A_head_light_blue_0" geometry={nodes.bot_A_head_light_blue_0.geometry} material={materials.light_blue} />
                      <mesh name="bot_A_head_face_01_0" geometry={nodes.bot_A_head_face_01_0.geometry} material={materials.face_01} />
                    </group>
                    <group name="bot_A_central_gear" position={[0, 4.006, 32.592]} rotation={[2.002, 0, 0]}>
                      <mesh name="bot_A_central_gear_bot_A_texture_0" geometry={nodes.bot_A_central_gear_bot_A_texture_0.geometry} material={materials.bot_A_texture} />
                      <mesh name="bot_A_central_gear_light_blue_0" geometry={nodes.bot_A_central_gear_light_blue_0.geometry} material={materials.light_blue} />
                    </group>
                    <group name="bot_A_wheels_1" position={[0, 6.703, 32.553]} rotation={[-1.974, 0, 0]}>
                      <mesh name="bot_A_wheels_1_bot_A_texture_0" geometry={nodes.bot_A_wheels_1_bot_A_texture_0.geometry} material={materials.bot_A_texture} />
                    </group>
                    <group name="bot_A_wheels_2" position={[0, 2.269, 35.041]} rotation={[-1.974, 0, 0]}>
                      <mesh name="bot_A_wheels_2_bot_A_texture_0" geometry={nodes.bot_A_wheels_2_bot_A_texture_0.geometry} material={materials.bot_A_texture} />
                    </group>
                    <group name="bot_A_wheels_3" position={[0, 2.269, 29.999]} rotation={[-2.002, 0, 0]}>
                      <mesh name="bot_A_wheels_3_bot_A_texture_0" geometry={nodes.bot_A_wheels_3_bot_A_texture_0.geometry} material={materials.bot_A_texture} />
                    </group>
                    <group name="tracks_01" position={[0, 7.269, 30.334]} rotation={[-Math.PI / 3, 0, 0]}>
                      <mesh name="tracks_01_bot_A_texture_0" geometry={nodes.tracks_01_bot_A_texture_0.geometry} material={materials.bot_A_texture} />
                    </group>
                    <group name="tracks_02" position={[0, 8.344, 31.33]} rotation={[-0.65, 0, 0]}>
                      <mesh name="tracks_02_bot_A_texture_0" geometry={nodes.tracks_02_bot_A_texture_0.geometry} material={materials.bot_A_texture} />
                    </group>
                    <group name="tracks_03" position={[0, 8.749, 32.719]} rotation={[0.075, 0, 0]}>
                      <mesh name="tracks_03_bot_A_texture_0" geometry={nodes.tracks_03_bot_A_texture_0.geometry} material={materials.bot_A_texture} />
                    </group>
                    <group name="tracks_04" position={[0, 8.096, 33.999]} rotation={[0.838, 0, 0]}>
                      <mesh name="tracks_04_bot_A_texture_0" geometry={nodes.tracks_04_bot_A_texture_0.geometry} material={materials.bot_A_texture} />
                    </group>
                    <group name="tracks_05" position={[0, 6.909, 34.874]} rotation={[Math.PI / 3, 0, 0]}>
                      <mesh name="tracks_05_bot_A_texture_0" geometry={nodes.tracks_05_bot_A_texture_0.geometry} material={materials.bot_A_texture} />
                    </group>
                    <group name="tracks_06" position={[0, 5.635, 35.619]} rotation={[Math.PI / 3, 0, 0]}>
                      <mesh name="tracks_06_bot_A_texture_0" geometry={nodes.tracks_06_bot_A_texture_0.geometry} material={materials.bot_A_texture} />
                    </group>
                    <group name="tracks_07" position={[0, 4.361, 36.364]} rotation={[1.048, 0, 0]}>
                      <mesh name="tracks_07_bot_A_texture_0" geometry={nodes.tracks_07_bot_A_texture_0.geometry} material={materials.bot_A_texture} />
                    </group>
                    <group name="tracks_08" position={[0, 3.007, 36.935]} rotation={[1.338, 0, 0]}>
                      <mesh name="tracks_08_bot_A_texture_0" geometry={nodes.tracks_08_bot_A_texture_0.geometry} material={materials.bot_A_texture} />
                    </group>
                    <group name="tracks_09" position={[0, 1.553, 36.904]} rotation={[1.921, 0, 0]}>
                      <mesh name="tracks_09_bot_A_texture_0" geometry={nodes.tracks_09_bot_A_texture_0.geometry} material={materials.bot_A_texture} />
                    </group>
                    <group name="tracks_10" position={[0, 0.484, 35.951]} rotation={[2.761, 0, 0]}>
                      <mesh name="tracks_10_bot_A_texture_0" geometry={nodes.tracks_10_bot_A_texture_0.geometry} material={materials.bot_A_texture} />
                    </group>
                    <group name="tracks_11" position={[0, 0.182, 34.513]} rotation={[3.129, 0, 0]}>
                      <mesh name="tracks_11_bot_A_texture_0" geometry={nodes.tracks_11_bot_A_texture_0.geometry} material={materials.bot_A_texture} />
                    </group>
                    <group name="tracks_12" position={[0, 0.176, 33.036]} rotation={[Math.PI, 0, 0]}>
                      <mesh name="tracks_12_bot_A_texture_0" geometry={nodes.tracks_12_bot_A_texture_0.geometry} material={materials.bot_A_texture} />
                    </group>
                    <group name="tracks_13" position={[0, 0.175, 31.56]} rotation={[Math.PI, 0, 0]}>
                      <mesh name="tracks_13_bot_A_texture_0" geometry={nodes.tracks_13_bot_A_texture_0.geometry} material={materials.bot_A_texture} />
                    </group>
                    <group name="tracks_14" position={[0, 0.244, 30.087]} rotation={[-3.095, 0, 0]}>
                      <mesh name="tracks_14_bot_A_texture_0" geometry={nodes.tracks_14_bot_A_texture_0.geometry} material={materials.bot_A_texture} />
                    </group>
                    <group name="tracks_15" position={[0, 0.742, 28.715]} rotation={[-2.539, 0, 0]}>
                      <mesh name="tracks_15_bot_A_texture_0" geometry={nodes.tracks_15_bot_A_texture_0.geometry} material={materials.bot_A_texture} />
                    </group>
                    <group name="tracks_16" position={[0, 1.97, 27.955]} rotation={[-1.64, 0, 0]}>
                      <mesh name="tracks_16_bot_A_texture_0" geometry={nodes.tracks_16_bot_A_texture_0.geometry} material={materials.bot_A_texture} />
                    </group>
                    <group name="tracks_17" position={[0, 3.407, 28.169]} rotation={[-1.121, 0, 0]}>
                      <mesh name="tracks_17_bot_A_texture_0" geometry={nodes.tracks_17_bot_A_texture_0.geometry} material={materials.bot_A_texture} />
                    </group>
                    <group name="tracks_18" position={[0, 4.712, 28.858]} rotation={[-1.049, 0, 0]}>
                      <mesh name="tracks_18_bot_A_texture_0" geometry={nodes.tracks_18_bot_A_texture_0.geometry} material={materials.bot_A_texture} />
                    </group>
                    <group name="tracks_19" position={[0, 5.995, 29.587]} rotation={[-1.056, 0, 0]}>
                      <mesh name="tracks_19_bot_A_texture_0" geometry={nodes.tracks_19_bot_A_texture_0.geometry} material={materials.bot_A_texture} />
                    </group>
                    <mesh name="bot_A_bot_A_texture_0" geometry={nodes.bot_A_bot_A_texture_0.geometry} material={materials.bot_A_texture} />
                    <mesh name="bot_A_light_blue_0" geometry={nodes.bot_A_light_blue_0.geometry} material={materials.light_blue} />
                  </group>
                  <group name="bot_C_1" position={[30.764, -0.099, 30.383]}>
                    <group name="bot_C" position={[0, 4.94, -30.223]} rotation={[-0.011, 0, 0]}>
                      <group name="bot_C_head" position={[0, 10.543, -3.003]} rotation={[-0.008, 0.021, -0.021]}>
                        <group name="bot_C_face" position={[0.003, 5.075, 5.676]}>
                          <mesh name="bot_C_face_face_01_0" geometry={nodes.bot_C_face_face_01_0.geometry} material={materials.face_01} />
                        </group>
                        <mesh name="bot_C_head_bot_C_texture_0" geometry={nodes.bot_C_head_bot_C_texture_0.geometry} material={materials.bot_C_texture} />
                        <mesh name="bot_C_head_face_01_0" geometry={nodes.bot_C_head_face_01_0.geometry} material={materials.face_01} />
                      </group>
                      <mesh name="bot_C_bot_C_texture_0" geometry={nodes.bot_C_bot_C_texture_0.geometry} material={materials.bot_C_texture} />
                    </group>
                    <group name="wheel_l_orientation" rotation={[0, 0, -0.066]}>
                      <group name="bot_C_wheel_left" position={[5.699, 5.271, -30.26]} rotation={[-0.628, 0, 0]}>
                        <mesh name="bot_C_wheel_left_bot_C_texture_0" geometry={nodes.bot_C_wheel_left_bot_C_texture_0.geometry} material={materials.bot_C_texture} />
                        <mesh name="bot_C_wheel_left_light_blue_0" geometry={nodes.bot_C_wheel_left_light_blue_0.geometry} material={materials.light_blue} />
                      </group>
                    </group>
                    <group name="wheel_r_orientation" position={[-12.858, 0, 0]} rotation={[0, 0, 0.066]}>
                      <group name="bot_C_wheel_right" position={[7.122, 4.419, -30.26]} rotation={[-0.628, 0, 0]}>
                        <mesh name="bot_C_wheel_right_bot_C_texture_0" geometry={nodes.bot_C_wheel_right_bot_C_texture_0.geometry} material={materials.bot_C_texture} />
                        <mesh name="bot_C_wheel_right_light_blue_0" geometry={nodes.bot_C_wheel_right_light_blue_0.geometry} material={materials.light_blue} />
                      </group>
                    </group>
                  </group>
                </group>
              </group>
            </group>
          </group>
        </group>
      </group>
    </Canvas>
  )
}

export default Robots;

useGLTF.preload('/robots/scene.gltf')
