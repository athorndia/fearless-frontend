window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/states/';
  
    const response = await fetch(url);
  
    if (response.ok) {
      const data = await response.json();
      const selectTag = document.getElementById('state');
    for (let state of data.states) {
        // Create an 'option' element
        const option = document.createElement("option");
        // Set the '.value' property of the option element to the
        // state's abbreviation
        option.value = state.abbreviation;
        // Set the '.innerHTML' property of the option element to
        // the state's name
        option.innerHTML = state.name;
        // Append the option element as a child of the select tag
        selectTag.appendChild(option);
    }
  }
});

/* 
This code listens for the DOMContentLoaded event, 
then makes a GET request to the specified URL. 
If the response is OK, it parses the JSON data and 
iterates through each state in the states array. 

For each state, it creates a new option element, 
sets its value and innerHTML properties based on 
the state's abbreviation and name, respectively, 
and appends the new option element as a child 
of the select element with the ID 'state'.
*/