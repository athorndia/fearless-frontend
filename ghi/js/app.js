// Card HTML
function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
            <div class="card mb-3 shadow-sm rounded h-100">
                <img src="${pictureUrl}" class="card-img-top" style="aspect-ratio:4/3" alt="image of city">
                <div class="card-body">
                    <h5 class="card-title">${name}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                    <p class="card-text">${description}</p>
                </div>
                <div class="card-footer text-muted">
                    ${new Date(starts).toLocaleDateString()} -
                    ${new Date(ends).toLocaleDateString()}
                </div>
            </div>
        </div>
    `;
}

// Placeholder Card HTML
function createPlaceholderCard(index) {
    return `
        <div class="col-12 col-md-6 col-lg-4 col-xl-3 p-2 placeholder-${index} mb-3">
            <div class="card mb-3 shadow-sm rounded" aria-hidden="true">
            <img class="card-img=top" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4
  //8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==" alt="placeholder">
                <div class="card-body">
                    <h5 class="card-title placeholder-glow">
                        <span class="placeholder col-6 bg-primary"></span>
                    </h5>
                    <h6 class="card-subtitle placeholder-glow mb-2 text-muted">
                        <span class="placeholder col-7 bg-secondary mb-3"></span> 
                    </h6>
                    <p class="card-text placeholder-glow">
                        <span class="placeholder col-7 bg-success"></span>
                        <span class="placeholder col-4 bg-success"></span>
                        <span class="placeholder col-4 bg-success"></span>
                        <span class="placeholder col-6 bg-success"></span>
                        <span class="placeholder col-8 bg-success"></span>
                    </p>
                </div>
                <div class="card-footer placeholder-glow text-muted">
                    <span class="placeholder col-4 bg-warning></span> -
                    <span class="placeholder col-4 bg-warning></span>
                </div>
            </div>
        </div>
    `;
}

function errorMessage(message) {
    return `<div class="alert alert-danger">${message}</div>`;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    // Define row
    const row = document.querySelector('.row');

    try {
        const response = await fetch(url);

        if (!response.ok) {
            // Figure out what to do when the response is bad.
            const column = document.querySelector('.error');
            const html = errorMessage(`${response.status}, ${response.statusText} - Could not load conferences`);
            column.innerHTML = html;
        } else {
            const data = await response.json();

            for (let i = 0; i < data.conferences.length; i++) {
                const html = createPlaceholderCard(i)
                const column = document.querySelector('.row');
                column.innerHTML += html;
            }

            for (let i = 0; i < data.conferences.length; i++) {
                const conference = data.conferences[i];
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const starts = details.conference.starts;
                    const ends = details.conference.ends;
                    const location = details.conference.location.name;
                    const html = createCard(title, description, pictureUrl, starts, ends, location);
                    const column = document.querySelector(`.placeholder-${i}`);
                    column.innerHTML = html;
                    column.classList.remove(`.placeholder-${i}`)
                }
            }
        }
    } catch (e) {
        console.error(e);
        // Figure out what to do if an error is raised.
        const column = document.querySelector('.error');
        const html = errorMessage(`${e}`);
        column.innerHTML = html;
    }
});


// need to format the cards similar to pinterest 